package myserver

import (
	"fmt"
	"net/http"
)

// Implement EndPoints
func (s *Server) handleIndex(msg string) http.HandlerFunc {
	msg = "Hi " + msg
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, msg)
	}
}

func (s *Server) handleRestricted() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "restricted")
	}
}

func (s *Server) handleAPI() http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, `
		{
		   "wgsLat": 35.362597218,
    		"wgsLong": 199.138391094
		}`)
	}
}
