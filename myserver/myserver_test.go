package myserver

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandleRoot(t *testing.T) {
	route := "/"
	srv := NewServer()
	srv.Route()
	req, _ := http.NewRequest("GET", route, nil)
	w := httptest.NewRecorder()
	srv.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("error in GET route %s.", route)
	}
}
