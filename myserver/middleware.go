package myserver

import (
	"fmt"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

// MiddleWare implementation for EndPoints
func (s *Server) MWAdminOnly(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		onFive := time.Now().Format("04")
		fmt.Println(onFive)
		if onFive != "5" {
			http.NotFound(w, r)
			return
		}
		h(w, r)
	}
}

// MiddleWare implemented for All Routes - refer to Gorilla Package
func (s *Server) MWLogging(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Do stuff here
		logrus.Println(r.RequestURI)
		// Call the next handler, which can be another middleware in the chain, or the final handler.
		h.ServeHTTP(w, r)
	})
}
