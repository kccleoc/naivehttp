package myserver

import (
	"github.com/gorilla/mux"
)

// https://medium.com/statuscode/how-i-write-go-http-services-after-seven-years-37c208122831
type Server struct {
	*mux.Router
}

// NewServer create new router using gorilla mux router
func NewServer() *Server {
	r := mux.NewRouter()
	return &Server{r}
}
