package myserver

import "net/http"

// Define the locations of http endpoints
func (s *Server) Route() {
	s.HandleFunc("/", s.handleIndex("Gopher")).Methods("GET")
	staticDir := "html/static"
	s.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(staticDir))))
	s.HandleFunc("/admin", s.MWAdminOnly(s.handleRestricted()))
	s.HandleFunc("/api", s.handleAPI()).Methods("GET")
}
