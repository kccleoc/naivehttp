package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/kccleoc/naivehttp/myserver"
	log "github.com/sirupsen/logrus"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "80"
		log.Printf("Defaulting to port %s", port)
	}

	// the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m
	var wait time.Duration = 15 * time.Second

	// define server
	s := myserver.NewServer()
	srv := &http.Server{
		Handler: s.Router,
		Addr:    ":" + port,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		IdleTimeout:  60 * time.Second,
	}
	s.Route()
	s.Use(s.MWLogging)

	// start server
	// Run our server in a goroutine so that it doesn't block.
	log.Printf("listening on 0.0.0.0:%s", port)
	// log.Println(srv.ListenAndServe())
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()
	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c
	log.Println("shutting down...")
	// Create Deadline to wait for after Ctrl-C
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()

	srv.Shutdown(ctx)
	log.Println("server closed gracefully")
	os.Exit(0)
}
